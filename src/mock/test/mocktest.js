import React from 'react';
import axios from 'axios';


export default class Mocktest extends React.Component{
    constructor(props){
        super(props);
        this.state={
            users:[]
        }
    }  
    componentDidMount(){
        this.request();
    }

    //动态获取mock数据
    request= ()=>{
        let baseUrl = "https://www.easy-mock.com/mock/5d3d586312924207a8eb7d69/hfut";
        axios.get(baseUrl + '/test').then((res)=>{
            if(res.status == '200'){
                console.log(res.data.result)
                this.setState({
                    users: res.data.result
                })
            }
        })
    }

    render(){
        return(
            <div>
                {
                    this.state.users.map((item)=>{
                        return(
                            <div>
                                <div>姓名：{item.name}</div>
                                <div>号码：{item.tel}</div>
                            </div>
                        )
                    })
                }
                {console.log("123")}
            </div>
        );
    }        
}