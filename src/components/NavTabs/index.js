import React from 'react';
import { Tabs } from 'antd';
import MenuConfig from '../../config/menuConfig';
import {HashRouter, Route, Link, Switch,NavLink} from 'react-router-dom'
const { TabPane } = Tabs;


export default class Students extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
          mode: 'top',
        };
      }
      handleModeChange = e => {
        const mode = e.target.value;
        this.setState({ mode });
      };
      componentWillMount(){
          const menuTreeNode = this.renderMenu(MenuConfig);
          this.setState({
              menuTreeNode
          })
      }
      //菜单渲染
      renderMenu= (data)=>{
        return data.map((item)=>{
            return (
                <TabPane tab={item.tab} key={item.key} >
                  {item.key}
                  <Link to="/home"></Link>
                </TabPane>
            )
        })
      }

      render() {
        const { mode } = this.state;
        return (
          <div>
            <Tabs defaultActiveKey="1" tabPosition={mode} >
              {this.state.menuTreeNode}
            </Tabs>
          </div>
        );
      }
}