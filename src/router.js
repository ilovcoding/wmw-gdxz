import React from 'react';
import {HashRouter, Route, Switch} from 'react-router-dom';
// import Students from './Students';
import Home from './pages/home';
import Delivery from './pages/delivery';
import Viewpoint from './pages/viewpoint';
import Growth from './pages/growth';
import Webculture from './pages/webculture';
import Service from './pages/service';
// import Test from './test';

export default class ERouter extends React.Component{
    render(){
        return (
            <HashRouter>
                {/* <Test> */}
                {/*
                  * @author 王旻伟
                  * 注释掉Test组件 原因:感觉Test组件在此处多余
                  * HashRouter中添加exact 原因: https://www.jianshu.com/p/bf6b45ce5bcc
                 */}
                    <Switch>
                        <Route path="/" exact={true} component={Home}></Route>                        
                        <Route path="/home" exact={true} component={Home}></Route>
                        <Route path="/delivery" exact={true} component={Delivery}></Route>
                        <Route path="/viewpoint" exact={true} component={Viewpoint}></Route>
                        <Route path="/growth"  exact={true} component={Growth}></Route>
                        <Route path="/webculture" exact={true} component={Webculture}></Route>
                        <Route path="/service" exact={true}  component={Service}></Route>
                    </Switch>
                {/* </Test> */}
            </HashRouter>
        );
    }
}