const menuList = [
    {
        tab: '首页',
        key: "home",
        the_url: '/home'
    },
    {
        tab: '校园速递',
        key: "delivery",
        the_url: '/delivery'
    },
    {
        tab: '思想观点',
        key: "viewpoint",
        the_url: '/viewpoint'
    },
    {
        tab: '学习成长',
        key: "growth",
        the_url: '/growth'
    },
    {
        tab: '网络文化',
        key: "webculture",
        the_url: '/webculture'
    },
    {
        tab: '生活服务',
        key: "service",
        the_url: '/service'
    }
]
export default menuList;