import React from 'react';
import ERouter from './router';
import Mytabs from './test'
class App extends React.Component{
 state= {
   TabsKey: '#/home'
 }
 componentWillMount(){
   this.setState({
      TabsKey: window.location.hash
   })
   console.log(new Date())
 }
 render () {
    return (
      <div>
        <div>
          <Mytabs  myTabsKey ={this.state.TabsKey}/>
        </div>
        <div>
          <ERouter />
        </div>
      </div>
    );
 }

}

export default App;
