import React from 'react';
import Tabs from './components/NavTabs';
export default class Students extends React.Component{
  render(){
    return(
      <div>
        <Tabs/>
        {this.props.children}
      </div>
    );
  }
}