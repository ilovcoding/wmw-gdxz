import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App'
// import Students from './Students';
// import Test from './test'
// import Mymock from './mock/test/mocktest'
import * as serviceWorker from './serviceWorker';
//原写法
// ReactDOM.render(<Test />, document.getElementById('root'));
//改后写法
ReactDOM.render(<App />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
