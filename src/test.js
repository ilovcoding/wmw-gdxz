import { Tabs} from 'antd';
import React from 'react';

// import Home from './pages/home';
// import Delivery from './pages/delivery';
// import Viewpoint from './pages/viewpoint';
// import Growth from './pages/growth';
// import Webculture from './pages/webculture';
// import Service from './pages/service';
import menuList from './config/menuConfig';
const { TabPane } = Tabs;
/**
 * @author WMW
 * 自定义组件记得首字母采用如Mytabs,万一使用mytabs则React dom 算法无法解析
 */
export default class Mytabs extends React.Component {
  handleChange = (key)=>{
    window.location.href = window.location.origin + key
  }
  render() {
    return (
      <div>
        <Tabs defaultActiveKey={this.props.myTabsKey} tabPosition="top" onChange={this.handleChange}>
          {menuList.map((value)=>(<TabPane tab={value.tab} key={`#/${value.key}`} style={{marginTop: '-16px'}} ></TabPane>))}
        </Tabs>
        {/* <Tabs defaultActiveKey="/home" tabPosition="top">
            <TabPane tab="首页" key="1"style={{marginTop:"-16px"}}>
              <Home/>
            </TabPane>
            <TabPane tab="校园速递" key="2"style={{marginTop:"-16px"}}>
              <Delivery/>
            </TabPane>   
            <TabPane tab="思想观点" key="3"style={{marginTop:"-16px"}}>
              <Viewpoint/>
            </TabPane>   
            <TabPane tab="学习成长" key="4"style={{marginTop:"-16px"}}>
              <Growth/>
            </TabPane>   
            <TabPane tab="网络文化" key="5"style={{marginTop:"-16px"}}>
              <Webculture/>
            </TabPane>   
            <TabPane tab="生活服务" key="6"style={{marginTop:"-16px"}}>
              <Service/>
            </TabPane>       
        </Tabs> */}
      </div>
    );
  }
}