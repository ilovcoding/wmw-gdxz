import React from 'react';
import "./index.less";
import {Link} from 'react-router-dom';
export default class Home extends React.Component{
    render(){
        return (
            <div>
                {/* 首页开头图片 */}                    
                <img src={"/home-img/home.png"} style={{height:'230px'}} />
                <div className="picture-div">
                    {/* 校园速递图片 */}
                    <div className="font-wrap">
                        <font className="font-left">校园速递</font> 
                        {/* <Link to="/delivery" className="a-left">了解更多 ></Link> */}
                    </div>
                    <img src={"/home-img/delivery.png"} /> 
                </div>
                <div className="picture-div">
                    {/* 思想观点图片 */}
                    <div className="font-wrap">
                        <font className="font-right">思想观点</font>
                        {/* <Link to="/viewpoint" className="a-right">了解更多 ></Link> */}
                    </div>
                    
                    <img src={'/home-img/viewpoint.png'} />
                </div>
                <div className="picture-div">
                    {/* 学习成长图片 */}
                    <div className="font-wrap">
                        <font className="font-left">学习成长</font>
                        {/* <Link to="/growth" className="a-left">了解更多 ></Link> */}
                    </div>
                    
                    <img src={'/home-img/growth.png'} />
                </div>
                <div className="picture-div">
                    {/* 网络文化图片 */}
                    <div className="font-wrap">
                        <font className="font-right">网络文化</font>
                        {/* <Link to="/webculture" className="a-right">了解更多 ></Link> */}
                    </div>
                    
                    <img src={'/home-img/webculture.png'} />
                </div>
                <div className="picture-div">
                    {/* 生活服务图片 */}
                    <div className="font-wrap">
                        <font className="font-left">生活服务</font>
                        {/* <Link to="/service" className="a-left">了解更多 ></Link> */}
                    </div>
                    
                    <img src={'/home-img/service.png'}/> 
                </div>  
            </div>
        );
    }
}