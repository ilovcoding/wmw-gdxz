import React from 'react';
import "./index.less";
export default class Delivery extends React.Component{
    render(){
        return (
            <div>
                {/* 通知公告内容 */}
                <div className="block">
                    <div className="title">
                        <div className="left">
                            <div className="blue-bar"> </div>
                            <h2 style={{lineHeight:"45px"}}>思政教育</h2>
                        </div>
                    </div>
                    <div className="content">
                        <div className="left"></div>
                        <div className="right"></div>
                    </div>
                    <div className="content">
                        <div className="left" style={{float:"right"}}></div>
                        <div className="right" style={{float:"left"}}></div>
                    </div>
                    <div className="content">
                        <div className="left"></div>
                        <div className="right"></div>
                    </div>
                </div>

                {/* 校园风采内容 */}
                <div className="block">
                    <div className="title">
                        <div className="left">
                            <div className="blue-bar"> </div>
                            <h2 style={{lineHeight:"45px"}}>人文科技</h2>
                        </div>
                            
                    </div>
                    <div className="content">
                        <div className="left"></div>
                        <div className="right"></div>
                    </div>
                    <div className="content">
                        <div className="left" style={{float:"right"}}></div>
                        <div className="right" style={{float:"left"}}></div>
                    </div>
                    <div className="content">
                        <div className="left"></div>
                        <div className="right"></div>
                    </div>
                </div>

                {/* 各种链接 */}
                <a href="#">合肥工业大学十九大精神贯彻学习网</a><br/>
                <a href="#">合肥工业大学文明网</a><br/>
                <a href="#">合肥工业大学我的中国梦网站</a><br/>
                <a href="#">合肥工业大学培育和践行社会主义价值观网站</a><br/>
                <a href="#">合肥工业大学读书活动</a><br/>
                <a href="#">合肥工业大学学风建设专题网站</a><br/><br/>
            </div>
        );
    }
}