import React from 'react';
import "./index.less";
export default class Delivery extends React.Component{
    render(){
        return (
            <div>
                {/* 通知公告内容 */}
                <div className="block">
                    <div className="title">
                        <div className="left">
                            <div className="blue-bar"> </div>
                            <h2 style={{lineHeight:"45px"}}>优秀导员</h2>
                        </div>
                        <div className="right">
                            <span style={{lineHeight:"45px"}}>更多></span>
                        </div>
                    </div>
                    <div className="content">
                        <div className="left"></div>
                        <div className="right"></div>
                    </div>
                    <div className="content">
                        <div className="left"></div>
                        <div className="right"></div>
                    </div>
                    <div className="content">
                        <div className="left"></div>
                        <div className="right"></div>
                    </div>
                </div>

                {/* 校园风采内容 */}
                <div className="block">
                    <div className="title">
                        <div className="left">
                            <div className="blue-bar"> </div>
                            <h2 style={{lineHeight:"45px"}}>标兵风采</h2>
                        </div>
                        <div className="right">
                            <span style={{lineHeight:"45px"}}>更多></span>
                        </div>
                            
                    </div>
                    <div className="content">
                        <div className="left"></div>
                        <div className="right"></div>
                    </div>
                    <div className="content">
                        <div className="left"></div>
                        <div className="right"></div>
                    </div>
                    <div className="content">
                        <div className="left"></div>
                        <div className="right"></div>
                    </div>
                </div>

                {/* 一周快讯内容 */}
                <div className="block">
                    <div className="title">
                        <div className="left">
                            <div className="blue-bar"> </div>
                            <h2 style={{lineHeight:"45px"}}>报告讲座</h2>
                        </div>
                        <div className="right">
                            <span style={{lineHeight:"45px"}}>更多></span>
                        </div>
                            
                    </div>
                    <div className="content">
                        
                    </div>
                    <div className="content">
                        
                    </div>
                    <div className="content">
                        
                    </div>
                </div>
               {/* 各种网站 */}
               <div className="a-div">
                   <a href="#">知乎</a>
                   <a href="#">虎嗅网</a>
                   <a href="#">百度传课</a>
                   <a href="#">豆瓣</a>
                   <a href="#">网易云课堂</a>
                   <a href="#">慕课网</a>
                   <a href="#" style={{marginBottom:"30px"}}>腾讯课堂</a>
               </div>
            
            </div>
        );
    }
}