import React from 'react';
import "./index.less";
export default class Home extends React.Component{
    render(){
        return (
            <div>
                <div className="picture-div">
                    {/* 校园速递图片 */}
                    <div>
                        <font className="font-right">
                            教学服务
                            <div>
                                <img src={"/service-img/icon1.png"} />{" "}
                                本科教学
                                <br/>
                                <img src={"/service-img/icon2.png"} />{" "}
                                研究生教学 
                            </div>
                        </font>
                        

                    </div>
                    <img src={"/service-img/teaching.png"} /> 
                </div>
                <div className="picture-div">
                    {/* 思想观点图片 */}
                    <div>
                        <font className="font-left">
                            资助服务
                            <div>
                                <img src={"/service-img/icon3.png"} />{" "}
                                资助中心
                                <br/>
                                <img src={"/service-img/icon4.png"} />{" "}
                                勤工助学
                            </div>
                        </font>
                        
                    </div>
                    
                    <img src={"/service-img/aid.png"} />
                </div>
                <div className="picture-div">
                    {/* 学习成长图片 */}
                    <div>
                        <font className="font-right">就业服务</font>
                        
                    </div>
                    
                    <img src={"/service-img/employment.png"} />
                </div>
                <div className="picture-div">
                    {/* 网络文化图片 */}
                    <div>
                        <font className="font-left">心理服务</font>
                        
                    </div>
                    
                    <img src={"/service-img/psychology.png"} />
                </div>
                <div className="picture-div">
                    {/* 生活服务图片 */}
                    <div>
                        <font className="font-right">后勤服务</font>
                    </div>
                    
                    <img src={"/service-img/logistics.png"}/> 
                </div>  
            </div>
        );
    }
}