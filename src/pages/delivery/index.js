import React from 'react';
import "./index.less";
export default class Delivery extends React.Component{
    render(){
        return (
            <div>
                {/* 通知公告内容 */}
                <div className="block">
                    <div className="title">
                        <div className="left">
                            <div className="blue-bar"> </div>
                            <h2 style={{lineHeight:"45px"}}>通知公告</h2>
                        </div>
                        <div className="right">
                            <span style={{lineHeight:"45px"}}>查看全部</span>
                        </div>
                    </div>
                    <div className="content">
                        <div className="left"></div>
                        <div className="right"></div>
                    </div>
                    <div className="content">
                        <div className="left"></div>
                        <div className="right"></div>
                    </div>
                    <div className="content">
                        <div className="left"></div>
                        <div className="right"></div>
                    </div>
                </div>

                {/* 校园风采内容 */}
                <div className="block">
                    <div className="title">
                        <div className="left">
                            <div className="blue-bar"> </div>
                            <h2 style={{lineHeight:"45px"}}>校园风采</h2>
                        </div>
                        <div className="right">
                            <span style={{lineHeight:"45px"}}>查看全部</span>
                        </div>
                            
                    </div>
                    <div className="content">
                        <div className="left"></div>
                        <div className="right"></div>
                    </div>
                    <div className="content">
                        <div className="left"></div>
                        <div className="right"></div>
                    </div>
                    <div className="content">
                        <div className="left"></div>
                        <div className="right"></div>
                    </div>
                </div>

                {/* 一周快讯内容 */}
                <div className="block">
                    <div className="title">
                        <div className="left">
                            <div className="blue-bar"> </div>
                            <h2 style={{lineHeight:"45px"}}>一周快讯</h2>
                        </div>
                        <div className="right">
                            <span style={{lineHeight:"45px"}}>查看全部</span>
                        </div>
                            
                    </div>
                    <div className="content">
                        <div className="left"></div>
                        <div className="right"></div>
                    </div>
                    <div className="content">
                        <div className="left"></div>
                        <div className="right"></div>
                    </div>
                    <div className="content">
                        <div className="left"></div>
                        <div className="right"></div>
                    </div>
                </div>
            </div>
        );
    }
}