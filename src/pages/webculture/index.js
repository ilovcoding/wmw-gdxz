import React from 'react';
import "./index.less";
export default class Delivery extends React.Component{
    render(){
        return (
            <div>
                {/* 导员博文内容 */}
                <div className="block">
                    <div className="title">
                        <div className="left">
                            <div className="blue-bar"> </div>
                            <h2 style={{lineHeight:"45px"}}>导员博文</h2>
                        </div>
                        <div className="right">
                            <span style={{lineHeight:"45px"}}>查看全部</span>
                        </div>
                    </div>
                    <div className="content">
                        <div className="left"></div>
                        <div className="right"></div>
                    </div>
                    <div className="content">
                        <div className="left"></div>
                        <div className="right"></div>
                    </div>
                    <div className="content">
                        <div className="left"></div>
                        <div className="right"></div>
                    </div>
                </div>

                {/* 校园风采内容 */}
                <div className="block">
                    <div className="title">
                        <div className="left">
                            <div className="blue-bar"> </div>
                            <h2 style={{lineHeight:"45px"}}>学子博文</h2>
                        </div>
                        <div className="right">
                            <span style={{lineHeight:"45px"}}>查看全部</span>
                        </div>
                            
                    </div>
                    <div className="content">
                        <div className="left"></div>
                        <div className="right"></div>
                    </div>
                    <div className="content">
                        <div className="left"></div>
                        <div className="right"></div>
                    </div>
                    <div className="content">
                        <div className="left"></div>
                        <div className="right"></div>
                    </div>
                </div>
            </div>
        );
    }
}